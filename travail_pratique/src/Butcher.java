// The Butcher class works similary to the milkman and the baker class.

public class Butcher implements Runnable {

    @Override
    public void run() {
        while (true) {
            // wait for bread and butter
            try {
                SandwichGourmets.hamProd.acquire();
                //System.out.println("butcher take advantage");
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block           
                e.printStackTrace();
            }
            //previous solution
            //            try {
            //                SandwichGourmets.grocer_semaphore.acquire();
            //                System.out.println("butcher take advantage");
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block           
            //                e.printStackTrace();
            //            }
            //            try {
            //                SandwichGourmets.bread.acquire();
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block
            //                e.printStackTrace();
            //            }
            //            try {
            //                SandwichGourmets.butter.acquire();
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block
            //                e.printStackTrace();
            //            }           
            System.out.println("Butcher adds ham and makes sandwich");
            SandwichGourmets.grocer_semaphore.release();
            System.out.println("Bucher eats his sandwich");
        }
    }
}
