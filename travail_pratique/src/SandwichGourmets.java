/**
 * The main class SandwichGourmet will manage creation and attribution of the 
 * differents parts of a sandwich. 
 * The problem to solve, is then on person is making his own sandwich, 
 * other persons can't take a part of that sandwich. So the solution is 
 * that each person must reserve the two sandwich part before making the
 * sandwich.
 * 
 * 
 * First solution had a deadlock.
 * Second is greatly inspired from the Cigarette smoker problem.
 * 
 * For the second solution, 3 other threads are added: butcher, baker and 
 * milkman employee (like pushers in Cigarette smoker problem).
 * 
 * 
 * @author      Breitenmoser Antoine <antoine.breitenmoser@edu.hefr.ch>
 * @version     1.1                   
 * @since       2012-04-20
 *
 * Completion time: about 150 min.
 *
 * Honor Code: I pledge that this program represents my own
 *   program code. I received help from no one in designing
 *   and debugging my program.
 *
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SandwichGourmets {
    public static Semaphore grocer_semaphore  = new Semaphore(1, false);
    public static Semaphore bread             = new Semaphore(0, true);
    public static Semaphore ham               = new Semaphore(0, true);
    public static Semaphore butter            = new Semaphore(0, true);
    public static Semaphore breadProd         = new Semaphore(0, true);
    public static Semaphore hamProd           = new Semaphore(0, true);
    public static Semaphore butterProd        = new Semaphore(0, true);
    public static Semaphore mutex             = new Semaphore(1, false);
//declaration of checkers  
    public static boolean isBread             = false;
    public static boolean isButter            = false;
    public static boolean isHam               = false;
    public static ExecutorService threadExecutor = Executors
            .newCachedThreadPool();

    public static void main(String[] args) {
        Grocer grocer                         = new Grocer();
        Baker baker                           = new Baker();
        Butcher butcher                       = new Butcher();
        Milkman milkman                       = new Milkman();
        BakerEmployee bakerEmployee           = new BakerEmployee();
        ButcherEmployee butcherEmployee       = new ButcherEmployee();
        MilkmanEmployee milkmanEmployee       = new MilkmanEmployee();
        threadExecutor.execute(grocer);
        threadExecutor.execute(baker);
        threadExecutor.execute(butcher);
        threadExecutor.execute(milkman);
        threadExecutor.execute(bakerEmployee);
        threadExecutor.execute(butcherEmployee);
        threadExecutor.execute(milkmanEmployee);
    }

}
