//this class monitors the acquiring of ham.

public class ButcherEmployee implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                SandwichGourmets.ham.acquire();
                SandwichGourmets.mutex.acquire();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (SandwichGourmets.isButter) {
                SandwichGourmets.isButter=false;
                SandwichGourmets.breadProd.release();
            }
            else if(SandwichGourmets.isBread){
                SandwichGourmets.isBread=false;
                SandwichGourmets.butterProd.release();
            }
            else 
                SandwichGourmets.isHam=true;
            SandwichGourmets.mutex.release();

        }
    }
}