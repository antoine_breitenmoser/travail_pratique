//this class monitors the acuiring of butter.
public class MilkmanEmployee implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                SandwichGourmets.butter.acquire();
                SandwichGourmets.mutex.acquire();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (SandwichGourmets.isBread) {
                SandwichGourmets.isBread=false;
                SandwichGourmets.hamProd.release();
            }
            else if(SandwichGourmets.isHam){
                SandwichGourmets.isHam=false;
                SandwichGourmets.breadProd.release();
            }
            else 
                SandwichGourmets.isButter=true;
            SandwichGourmets.mutex.release();
        }
    }
}