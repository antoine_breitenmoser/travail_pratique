// The Milkman class works similary to the butcher and baker class.

public class Milkman implements Runnable {

    @Override
    public void run() {
        while (true) {
            // wait for bread and ham  
            try {
                SandwichGourmets.butterProd.acquire();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block           
                e.printStackTrace();
            }
            //previous solution
            //            try {
            //                SandwichGourmets.grocer_semaphore.acquire();
            //                System.out.println("milkman take advantage");
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block           
            //                e.printStackTrace();
            //            }
            //            try {
            //                SandwichGourmets.bread.acquire();
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block
            //                e.printStackTrace();
            //            }
            //            try {
            //                SandwichGourmets.ham.acquire();
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block
            //                e.printStackTrace();
            //            }
            System.out.println("Milkman adds butter and makes sandwich");
            SandwichGourmets.grocer_semaphore.release();
            System.out.println("Milkman eats his sandwich");
        }
    }
}
