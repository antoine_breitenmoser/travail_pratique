//The baker class works similary to butcher and the milkman class.


public class Baker implements Runnable {

    @Override
    public void run() {
        while (true) {
            // wait for ham and butter
            try {
                SandwichGourmets.breadProd.acquire();
                //System.out.println("baker take advantage");
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block           
                e.printStackTrace();
            }
            // previous solution
            //            try {
            //                SandwichGourmets.grocer_semaphore.acquire();
            //                System.out.println("baker take advantage");
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block           
            //                e.printStackTrace();
            //            }
            //            try {
            //                SandwichGourmets.ham.acquire();
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block
            //                e.printStackTrace();
            //            }
            //            try {
            //                SandwichGourmets.butter.acquire();
            //            } catch (InterruptedException e) {
            //                // TODO Auto-generated catch block
            //                e.printStackTrace();
            //            }
            System.out.println("Baker adds bread and makes sandwich");
            SandwichGourmets.grocer_semaphore.release();
            System.out.println("Baker eats his sandwich");
        }
    }

}
