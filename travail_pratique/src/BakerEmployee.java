//this class monitors the acquiring of bread.

public class BakerEmployee implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                SandwichGourmets.bread.acquire();
                SandwichGourmets.mutex.acquire();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (SandwichGourmets.isButter) {
                SandwichGourmets.isButter=false;
                SandwichGourmets.hamProd.release();
            }
            else if(SandwichGourmets.isHam){
                SandwichGourmets.isHam=false;
                SandwichGourmets.butterProd.release();
            }
            else 
                SandwichGourmets.isBread=true;
            SandwichGourmets.mutex.release();
        }
    }
}